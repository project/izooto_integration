<?php

namespace Drupal\izooto_integration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class IzootoConfigurationForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'izooto_integration.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'izooto_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $izooto_config = $this->config(static::SETTINGS);

    $form['izooto_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Izooto Code:'),
      '#default_value' => $izooto_config->get('izooto_code'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('izooto_code_form', $form_state->getValue('izooto_code'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
